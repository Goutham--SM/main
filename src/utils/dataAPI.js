import { _getUsers, _getQuestions, _saveQuestionAnswer, _saveQuestion } from './_DATA';

export function getAllData(){
  return Promise.all([
    _getUsers(),
    _getQuestions(),
  ]).then(([users,questions]) => ({
          users,
    	  questions,
  }));
}

export function saveQuestion(q){
  return _saveQuestion(q);
}

export function saveQuestionAnswer(authUser, qid, answer){
  return _saveQuestionAnswer({authUser, qid, answer});
}