import {RECIEVE_USERS, ADD_QUESTION_TO_USER, ADD_ANSWER} from '../actions/users';

export default function user(state={},action){
  
  switch(action.type){
    case RECIEVE_USERS:
      return {
        ...state,
        ...action.users,
      };
      
      case ADD_QUESTION_TO_USER:
      const { id, author } = action;

      return {
        ...state,
        [author]: {
          ...state[author],
          questions: state[author].questions.concat(id)
        }
      };
      
    case ADD_ANSWER:
      const { au, qid, ans } = action;

      return {
        ...state,
        [au]: {
          ...state[au],
          answers: {
            ...state[au].answers,
            [qid]: ans
          }
        }
      };

    default:
      return state;
  }
}
