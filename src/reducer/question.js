import {RECIEVE_QUESTIONS, ADD_QUESTIONS, ADD_VOTES} from '../actions/questions';

export default function question(state={},action){
  
  switch(action.type){
    case RECIEVE_QUESTIONS:
      return {
        ...state,
        ...action.questions,
      }
    case ADD_QUESTIONS:
      return{
      	...state,
        [action.question.id]:action.question,
      }
    case ADD_VOTES:
      const {authUser, qid, answer}=action;
      return{
        ...state,
        [qid]: {
          ...state[qid],
            [answer]:{
              ...state[qid][answer],
            votes: state[qid][answer].votes.concat(authUser)
          }
      	}
      }
    default:
      return state
  }
}
