import {SET_AUTORIZED_USER} from '../actions/authorizedUser';

export default function aU(state=null, action){
  
  switch(action.type){
    case SET_AUTORIZED_USER:
      return action.id;
    default :
      return state;
  }
}