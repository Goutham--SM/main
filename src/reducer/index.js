import {combineReducers} from 'redux';
import question from './question';
import user from './user';
import aU from './autorizedUser';

export default combineReducers({
	question,
  	user,
  	aU
})