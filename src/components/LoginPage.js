import React from 'react';
import {setAuthorizedUser} from '../actions/authorizedUser';
import {connect} from 'react-redux';
function LoginPage (props) {
  	   
  	
      const {userList} = props;

	  const selectedUser = (e)=>{
        e.preventDefault();
        const {dispatch} = props;
        dispatch(setAuthorizedUser(document.getElementById('suser').value));
      }
    
  
    return(
      <div className='signin'>
      	<h1 className='center title'>Please Signin</h1>
      	<select id="suser">
      {userList.map((u,i) => (
      	<option key={i}>{u}</option>
    	))}
            
      	</select> <br/>
      	<button onClick={selectedUser} className='btn'>Login</button>
		</div>
      );
  }

export default connect()(LoginPage);