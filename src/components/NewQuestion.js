import React from 'react';
import {connect} from 'react-redux';
import {handleAdd} from '../actions/questions';
import {Redirect} from 'react-router-dom';

class NewQuestion extends React.Component{
  
  	state = {
      one: '',
      two: '',
      isRedirected: false
    };
	handleOneChange = (e) =>{
    	const one = e.target.value;
      
      	this.setState(()=>({
        	one
        }));
    }

	handleTwoChange = (e) =>{
    	const two = e.target.value;
      
      	this.setState(()=>({
        	two
        }));
    }

	handleSubmit =(e) =>{
    	e.preventDefault();
      	const {one,two}=this.state;
      	console.log('The new options are:', one, two);
      
      	const {dispatch,aU} =this.props;
      	console.log(aU);
      	dispatch(handleAdd(one,two,aU));
      
      	this.setState(()=>({
        	one:'',
          	two:'',
          	isRedirected: true
        }));
      
    }
	render(){
      const {one,two, isRedirected} = this.state;
		if(isRedirected===true){
        	return <Redirect to='/'/>
        }
    	return(
        	<div>
          		<h2>Post a new question</h2>
             	<form className='new-question' onSubmit={this.handleSubmit}>
					<h1>Would you Rather?</h1>
             		<input type='text' placeholder='Option One' value={one} onChange={this.handleOneChange} />
					<h1>OR</h1>
             		<input type='text' placeholder='Option Two' value={two} onChange={this.handleTwoChange} />
					<button className='btn' type='submit' disabled={one==='' || two===''}>Post</button>
             	</form>
          	</div>        
        );
    };
}
function mapStateToProps({aU}){
	return {
    	aU
    };
}
export default connect(mapStateToProps)(NewQuestion);