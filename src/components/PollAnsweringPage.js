import React from 'react';
import {connect} from 'react-redux';
import {handleSaveQuestionAnswer} from '../actions/users';
import {Redirect} from 'react-router-dom';

class PollAnsweringPage extends React.Component{
  state={
    option:'none',
    isRedirected:false,
    idExists:true,
  }
  
  handleClick = (e)=>{
    this.setState((ps)=>({
      option: e.target.value}));
  }
  handleSubmit =(e) =>{
    e.preventDefault();
    const {option}=this.state;

    const {dispatch,id,aU} =this.props;
    
    dispatch(handleSaveQuestionAnswer(aU,id,option));

    this.setState(()=>({
      isRedirected: true
    }));
      
    }
  render(){
	if(this.state.isRedirected===true){
        	return <Redirect to='/'/>
        }
    if(this.state.idExists===false){
    	return <Redirect to='question/:id_notExist' />
    }
   
	return(
      <div>
      { !this.props.quesDisp.optionOne.votes.includes(this.props.activeUser.id) && !this.props.quesDisp.optionTwo.votes.includes(this.props.activeUser.id)?
      <div>
    	<div className='question'>
			<div className='question-left'>
			<img src={this.props.avatar} alt={this.props.author} className="avatar"/>
			<h3>{this.props.author.name}</h3>
			</div>
			<div className='question-right'>
                <h2>Would you rather?</h2>
				<div>
                <input type="radio" name='option' id='o1' value='optionOne' onChange={this.handleClick}/>
                  <label htmlFor='o1'>{this.props.quesDisp.optionOne.text}</label>
				</div>
				<div>
                  <input type="radio" name='option' id='o2' value='optionTwo' onChange={this.handleClick}/>
                  <label htmlFor='o2'>{this.props.quesDisp.optionTwo.text}</label>
				</div>
			</div>
      	</div>
    	<button className='btn' id='bt' disabled={this.state.option==='none'?true:false} onClick={this.handleSubmit}>Submit</button>
    </div>
    	:
        <div className='card lbs'>
        <div className='question'>
          <div className='question-info lb'>
			<img src={this.props.avatar} alt={this.props.author} className="avatar"/>
			<h3>{this.props.author.name}</h3>
			</div>
			<div className='question-info lb'>
                <h2>Would you rather?</h2>
                <h3>{this.props.quesDisp.optionOne.text} <div className='ds' id='s'>{this.props.quesDisp.optionOne.votes.length} of 3 voted</div></h3>
                <h3>OR</h3>
                <h3>{this.props.quesDisp.optionTwo.text} <div className='ds' id='r'>{this.props.quesDisp.optionTwo.votes.length} of 3 voted</div></h3>
			</div>
      </div>
    </div>}
    	</div>
    );
  };
}

function mapStateToProps({aU, user, question}, props){
  	const {id}= props.match.params;
	const quesDisp = question[id];
  	return {
      	aU,
    	quesDisp,
      	author: user[quesDisp.author],
      	activeUser: user[aU],
      	avatar: user[quesDisp.author].avatarURL,
      	id
    };
}


export default connect(mapStateToProps)(PollAnsweringPage);