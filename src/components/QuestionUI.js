import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

class QuestionUI extends React.Component{

  render(){
	
	return(
    	<Link className='question'  to={`/questions/${this.props.ids}`}>
			<div className='question-left'>
			<img src={this.props.avatar} alt={this.props.author} className="avatar"/>
			<h3>{this.props.author.name}</h3>
			</div>
			<div className='question-right'>
                <h2>Would you rather?</h2>
                <h3>{this.props.quesDisp.optionOne.text}</h3>
                <h3>OR</h3>
                <h3>{this.props.quesDisp.optionTwo.text}</h3>
			</div>
      	</Link>
    );
  };
}
function mapStateToProps({aU, user, question}, {id}){
	const quesDisp = question[id];
  	return {
      	aU,
    	quesDisp,
      	author: user[quesDisp.author],
      	activeUser: user[aU],
      	avatar: user[quesDisp.author].avatarURL,
      	ids:quesDisp.id
    };
}

export default connect(mapStateToProps)(QuestionUI);