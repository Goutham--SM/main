
import React, { Component } from 'react';
import { BrowserRouter, Route,Switch} from "react-router-dom";
import LoginPage from './LoginPage';
import Title from './Title';
import HomePage from './HomePage';
import NewQuestion from './NewQuestion';
import NavBar from './NavBar';
import PollAnsweringPage from './PollAnsweringPage';
import NoMatch from './NoMatch';
import {handleAllData} from '../actions/index';
import {connect} from 'react-redux';
import LeaderBoard from './LeaderBoard';
class App extends Component {
	
	componentDidMount() {
    // get books on load
    this.props.dispatch(handleAllData());
  	}
	

	
  render() {
    return (
    <BrowserRouter>
      <div className="App center">
      	<Title/>
      	{
      	this.props.load === true?
      	<div>
      	<Switch>
     	<Route path="/" render={()=>(
    	<div>
      		<LoginPage userList={this.props.userID}/>
		</div>
			 )}/>
			<Route component={NoMatch}/>
			</Switch>
			</div>:
			<div>
		  <NavBar/>
			<Switch>
              <Route exact path='/' component={HomePage}/>
              <Route path='/questions/:id' component={PollAnsweringPage}/>
			  <Route path='/leaderboard' component={LeaderBoard}/>
              <Route path='/add' component={NewQuestion}/>
			  <Route component={NoMatch}/>

			</Switch>
		</div>
		}
      </div>
		
    </BrowserRouter>
    );
  }
}

function mapStateToProps({user, aU}){
  return {
    userID: Object.keys(user),
    load: aU===null,
  };
}

export default connect(mapStateToProps)(App);
