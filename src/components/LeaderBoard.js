import React from 'react';
import {connect} from 'react-redux';

class LeaderBoard extends React.Component{

  render(){
    
    return(
      <div>
      <h2>Leaderboard</h2>
      {this.props.data.map((d,i)=>(
        <div className='card' key={i} id={i}>
        <div className='question'>
		<div className='lb'>
      		<h1 className='center title'>{d.userName}</h1>
            <img className='avatar' src={d.userPic} alt={d.name}/>
		</div>
		<div className='lb'>
            <h4 id='ans'>Number of questions answered: {d.numAnswered}</h4>
			{i===0?<h4>Winner</h4>:i===1?<h4>Runner</h4>:<h4>3rd Place</h4>}
            <h4 id='q'>Number of questions created: {d.numCreated}</h4>
		</div>
		<div className='lb'>
            <h2>Total: {d.totalPoints}</h2>
		</div>
		</div>
        </div> ))}
      </div>        
      );
	};
}
function mapStateToProps({user}){
  const data = Object.values(user).map(u=>({
  	userId: u.id,
    userName: u.name,
    userPic:u.avatarURL,
    numAnswered: Object.keys(u.answers).length,
    numCreated: u.questions.length,
    totalPoints: u.questions.length + Object.keys(u.answers).length
  })).sort((a, b) => a.totalPoints - b.totalPoints).reverse();
	return {
      	data
    };
}
export default connect(mapStateToProps)(LeaderBoard);
  
