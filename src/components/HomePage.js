import React from 'react';
import {connect} from 'react-redux';

import  QuestionUI from './QuestionUI.js';

class HomePage extends React.Component{
  state={
    answered:false,
  }
  
 handleClickUnanswered =(e) =>{
  	e.preventDefault();
    console.log('The value of button',e.target.value);
   	e.target.value==='answered'?
    this.setState((ps)=>({
      answered:true}))
    :this.setState((ps)=>({
      answered:false}));
  }
  render(){
    console.log(this.props);
	
	const answeredQuestionId=Object.keys(this.props.activeUser.answers);
  return(
  	<div>
    	<h1>All Polls</h1>
    	<button className='btn' value='answered' onClick={this.handleClickUnanswered}>Answered</button>
    	<button className='btn' value='unanswered' onClick={this.handleClickUnanswered}>Unanswered</button>
    	<ul>
    		{this.state.answered?
              this.props.questionID.map((id)=>(id&&answeredQuestionId.includes(id)?
  				  <li key={id}>
                    <QuestionUI id={id}/>
                  </li>:null
  			)):this.props.questionID.map((id)=>(!(id&&answeredQuestionId.includes(id))?
  				  <li key={id}>
                    <QuestionUI id={id}/>
                  </li>:null))}	
    	</ul>
    </div>
  );
  }
}

function mapStateToProps({aU,user,question}){
  return {
    aU,
    activeUser:user[aU],
  	questionID: Object.keys(question).sort((a,b)=>question[b].timestamp-question[a].timestamp)
  };
}

export default connect(mapStateToProps)(HomePage);