import React from 'react';

function NoMatch(){
  return(
    <h1> Not Found - Bad URL </h1>
  );
}

export default NoMatch;