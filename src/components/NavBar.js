import React from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {setAuthorizedUser} from '../actions/authorizedUser';
function NavBar (props){
  const currentUser = props.activeUser;
  console.log('active user is:', props.aU);
  console.log('currentUSer is:', currentUser);
  const handleClick = (e)=>{
      props.dispatch(setAuthorizedUser(null));
  }
	return(
    	<nav className='nav'>
      		<ul>
      			<li>
      				<NavLink exact to='/'>
      					Home
      				</NavLink>
      			</li>
      			<li>
      				<NavLink to='/add'>
      					New Question
      				</NavLink>
      			</li>
      			<li>
      				<NavLink to='/leaderboard'>
      					LeaderBoard
      				</NavLink>
      			</li>
      			<li className='rightpush'>
      				<NavLink exact to='/'>
      					<div className='oneline'>
      						<img src={currentUser.avatarURL} alt={currentUser.name} className="avatarsmall"/>
							Welcome {currentUser.name}!
						</div>
      				</NavLink>
					</li>
					<li>
      				<NavLink to='/' onClick={handleClick}>
      					SignOut
      				</NavLink>
      			</li>
      		</ul>
      	</nav>
    );
}
function mapStateToProps({aU,user}){
  return{
    aU,
    activeUser: user[aU],
  };
}
export default connect(mapStateToProps)(NavBar);