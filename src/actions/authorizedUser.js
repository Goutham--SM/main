export const SET_AUTORIZED_USER = 'SET_AUTORIZED_USER';

export function setAuthorizedUser(id){
   return {
     type:SET_AUTORIZED_USER,
     id,
};
}