import {saveQuestion} from '../utils/dataAPI';
import {addQuestionToUser} from './users.js';
export const RECIEVE_QUESTIONS = 'RECIEVE_QUESTIONS';
export const ADD_QUESTIONS = 'ADD_QUESTIONS';
export const ADD_VOTES = 'ADD_VOTES_TO_QUESTION';

function addQuestion(question){
  	return {
    	type: ADD_QUESTIONS,
      	question,
    };
 }


export function recieveQuestions(questions){
  return {
    type: RECIEVE_QUESTIONS,
    questions,
  };
}

export function addVotesToQuestion(authUser, qid, answer){
  return {
    type: ADD_VOTES,
    authUser, 
    qid, 
    answer
  };
}
  
  export function handleAdd(optionOneText, optionTwoText, author){
    console.log(author);
  	return (dispatch) => {
      
      	return saveQuestion({optionOneText, optionTwoText, author}).then((question)=>{
          dispatch(addQuestion(question));
          dispatch(addQuestionToUser(question));
        });
    };
  }



