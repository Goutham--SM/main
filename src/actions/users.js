export const RECIEVE_USERS = 'RECIEVE_USERS';
export const ADD_QUESTION_TO_USER = 'ADD_QUESTION_TO_USER';
export const ADD_ANSWER = 'ADD_ANSWER_TO_USER';
import {saveQuestionAnswer} from '../utils/dataAPI';
import { addVotesToQuestion } from './questions';

export function recieveUsers(users){
  return {
    type: RECIEVE_USERS,
    users,
  };
}


export function addQuestionToUser({ id, author }) {
  return {
    type: ADD_QUESTION_TO_USER,
    id,
    author
  };
}

function addAnswerToUser(au, qid, ans) {
  return {
    type: ADD_ANSWER,
    au, 
    qid, 
    ans
  };
}

export function handleSaveQuestionAnswer(authedUser,qid, answer) {
  console.log(answer);
  return (dispatch) => {
    dispatch(addAnswerToUser(authedUser, qid, answer));
    dispatch(addVotesToQuestion(authedUser, qid, answer));
    saveQuestionAnswer(authedUser, qid, answer);
  };
}