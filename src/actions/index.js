import {getAllData} from '../utils/dataAPI';
import {recieveQuestions} from './questions';
import {recieveUsers} from './users';
import {setAuthorizedUser} from './authorizedUser';


export function handleAllData(){
  return(dispatch) =>{
    return getAllData().then(({users,questions}) => {
    	dispatch(recieveQuestions(questions));
        dispatch(recieveUsers(users));
    });
  };
}

export function setActiveUser(id){
  return(dispatch)=>{
      dispatch(setAuthorizedUser(id));
  };
}