#Would you Rather?

##The person using application will be impersonating/logging in as an existing user and make sure all the deatails about logged in user is stored, so that information about the logged in user appears on the page. If someone tries to navigate anywhere by entering the address in the address bar, the user is asked to sign in and then the requested page is shown. The application allows the user to log out and log back in.

##Once the user logs in, the user should be able to toggle between his/her answered and unanswered polls on the home page, which is located at the root. The polls in both categories are arranged from the most recently created (top) to the least recently created (bottom). The unanswered questions are shown by default.

##Each polling question should link to the details of that poll. Which gives all the details about the number of votes per answer, who created the poll and the options for that poll. To be precise each poll displays the following when clicked:

###Text “Would You Rather”;
Avatar of the user who posted the polling question; and
Two options.
For answered polls, each of the two options contains the following:

###Text of the option;
Number of people who voted for that option; and
Percentage of people who voted for that option.
The option selected by the logged-in user is clearly marked.

##The application should show a 404 page if the user is trying to access a poll that does not exist. A navigation bar is also available so that the user can easily navigate anywhere in the application.

##So what happens when someone votes in a poll? Upon voting in a poll, all of the information of an answered pollis displayed. The user’s response is recorded and clearly visible on the poll details page. Users can only vote once per poll and aren't allowed to change their answer after they’ve voted hen the user comes back to the home page, the polling question moves to the “Answered” column.

##The form for posting new polling questions is also available which has a form for creating two options. Upon submitting the form, a new poll is created, and the user is taken to the home page.

##To encourage some healthy competition! The application shows a leaderboard that contains the following:

###User’s name;
User’s picture;
Number of questions the user asked; and
Number of questions the user answered
##Users are ordered in descending order based on the sum of the number of questions they’ve asked and the number of questions they’ve answered. The more questions you ask and answer, the higher up you move.

##Steps to install and run the application:
###1. Open the command prompt or bash or terminal and move to the directory of the folder.
###2. Type npm install to install the app.
###3. Once the installation process is done, type npm start to make the app run.
####NOTE: The app runs in a http://localhost:3000/